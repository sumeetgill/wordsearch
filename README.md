# README #

This was a coding test for an interview that I have done.

# Instructions:#

Write a program which displays all the words that can be made by using any subset of the letters in a provided word.

Your program should read the dictionary of valid words from standard input.  Words will be one per line.  Ignore whitespace.  We will provide our own input dictionary, but as a testing suggestion, you can use the one often found on Unix systems at /usr/dict/words or /usr/share/dict/words.

The word whose letters will be used to make other words will be provided as the first (and only) argument to your program.  We will call this the master word.

Your program should output all the words from the input dictionary that can be made by using any subset of the letters in the master word.  Each word should appear only once in the output, but the order of the words in the output is irrelevant.  Note that each letter in the master word can only be used once, so that you cannot make the dictionary word "good" from the master word "dog".  When the master word repeats a letter more than once, you may use that letter up to as many times as it appears in the master word.

1.	Take your time, and make a solution that is correct, clean, simple, and easy to read/understand.  Show us your ability to organize and communicate ideas in code.
2.	Make your solution robust: deal with corner cases appropriately.
3.	You may use any language you are comfortable with, but we prefer to see solutions in a compiled, statically-typed language such as Java, C, C++, etc. since those are the types of languages we work with at [redacted].
4.	You may use anything from your language's standard libraries, but don't use anything that would make the problem trivial such that we cannot see enough of a sample of your personal code.
5.	If you wish to include tests, please do so, but it is up to you how you wish to gain confidence in the correctness of your program.
6.	Submit your code and any tests, build procedures, documentation, etc. by email in an attached zipfile or tarball.
7.	We only have Linux and Mac machines to run your code, so don't include a dependency on a different OS (e.g. Windows) or on an exotic compiler or interpreter that we can't just download from the web.
8.	If you have questions, please ask us.  We are happy to help clarify anything.