//
//  SearchController.swift
//  WordSearch
//
//  Created by Sumeet Gill on 2017-03-26.
//  Copyright © 2017 Sumeet Gill. All rights reserved.
//

import Foundation


//MARK: - Class variables
var wordDictionary:[String:NSCountedSet] = [:]


//MARK: - Set up method

/**
  Iterates through text file and inputs into an array
 
 - Parameter: N/A
 
 - Returns: Bool for success or failure
 */
func IndexWordsToArray() -> Bool {
    
    var dictionaryArray:[String] = [String]()
    
    if let path = Bundle.main.path(forResource: "words", ofType: "txt") {
        do {
            let data = try String(contentsOfFile: path, encoding: .utf8).lowercased()
            let wordArray = data.components(separatedBy: .newlines)
            dictionaryArray = wordArray
        } catch {
            NSLog("Error: Failed to import txt to array")
            return false
        }
    }
    
    for word in dictionaryArray {
        let wordSet = CountedSetFromString(inputString: word as String)
        wordDictionary[word] = wordSet
    }
    
    NSLog("Index Complete")
    return true
}

//MARK: - Utility methods

/**
 Converts an input String into a CountedSet
 
 - Parameter inputString: A string representation of user search term
 
 - Returns: NSCountedSet optional
 */
func CountedSetFromString(inputString: String) -> NSCountedSet? {
    
    guard inputString.characters.count > 0 else {
        NSLog("Error: Input is empty for CountedSetFromString")
        return nil
    }
    
    guard inputString.rangeOfCharacter(from: CharacterSet.letters) != nil else {
        NSLog("Error: Query String must be letters.")
        return nil
    }
    
    let strSet = NSCountedSet()
    let lowerCasedInput = inputString.lowercased()
    
    for chars in lowerCasedInput.characters {
        strSet.add(String(chars))
    }
    
    return strSet
}

//MARK: Querying methods

/**
  Iterates through the NSDictionary of words and outputs the matching results.
 
 - Parameter queryString: A String to be used to compare to dictionary words
 
 - Returns: Bool for success or failure
 */
func QueryDictionary(queryString: String) -> Array<Any>? {
    
    guard queryString.characters.count > 0 else {
        NSLog("Error: Query String cannot be blank.")
        return nil
    }
    
    guard queryString.rangeOfCharacter(from: CharacterSet.letters) != nil else {
        NSLog("Error: Query String must be letters.")
        return nil
    }
    
    if let querySet = CountedSetFromString(inputString: queryString) {
        let wordDict = wordDictionary as NSDictionary
        let results = wordDict.allKeys(for: querySet)
        
        NSLog("Search Complete")
        return results
    }
    
    NSLog("Search completed with no results")
    return nil
}
