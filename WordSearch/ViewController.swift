//
//  ViewController.swift
//  WordSearch
//
//  Created by Sumeet Gill on 2017-03-26.
//  Copyright © 2017 Sumeet Gill. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UISearchBarDelegate {

    @IBOutlet var sbWordSearch: UISearchBar!
    @IBOutlet var txtResults: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sbWordSearch.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        txtResults.text = ""
        
        if let searchText = searchBar.text {
            if let results = QueryDictionary(queryString: searchText) {
                var resultString:String = ""
                
                for word in results {
                    resultString += "• " + String(describing: word).capitalized + "\n"
                }
                
                txtResults.text = resultString
            }
        }
    }

    
}

