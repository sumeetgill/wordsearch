//
//  WordSearchTests.swift
//  WordSearchTests
//
//  Created by Sumeet Gill on 2017-03-26.
//  Copyright © 2017 Sumeet Gill. All rights reserved.
//

import XCTest
@testable import WordSearch

class WordSearchTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    //MARK: - CountedSetFromString
    
    func testCountedSetFromString_EmptyInput() {
        let testSet = CountedSetFromString(inputString: "")
        
        XCTAssert(testSet == nil)
    }
    
    func testCountedSetFromString_NumericInput() {
        let testSet = CountedSetFromString(inputString: "22222")
        
        XCTAssert(testSet == nil)
    }
    
    func testCountedSetFromString_PassingInput() {
        let testSet = CountedSetFromString(inputString: "test")
        
        XCTAssert(testSet != nil)
    }
    
    //MARK: - QueryDictionary
    
    func testQueryDictionary_EmptyString() {
        let testBool = QueryDictionary(queryString: "")
        
        XCTAssert(testBool == nil)
    }
    
    func testQueryDictionary_NumericString() {
        let testBool = QueryDictionary(queryString: "2222")
        
        XCTAssert(testBool == nil)
    }
    
    func testQueryDictionary_PassingInput() {
        let testBool = QueryDictionary(queryString: "test")
        
        XCTAssertNotNil(testBool)
    }
}
